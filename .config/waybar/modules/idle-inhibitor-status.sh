#!/usr/bin/env bash

status=$(systemctl --user is-active idle-inhibit.service)
if [[ "${status}" == "active" ]]
then
    echo -e "{\"text\":\"active\",\"alt\":\"active\",\"tooltip\":\"active\",\"class\":\"idle_inhibitor\"}"
else
    echo -e "{\"text\":\"inactive\",\"alt\":\"inactive\",\"tooltip\":\"inactive\",\"class\":\"idle_inhibitor\"}"
fi
