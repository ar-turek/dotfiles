#!/usr/bin/env bash

username=$(whoami)
full_name=`getent passwd ${username} | cut -d ':' -f 5 | cut -d ',' -f 1`
echo -e "{\"text\":\"${full_name} (${username})\", \"class\":\"powermenu\"}"
