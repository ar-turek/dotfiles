alias rm="rm --force"
alias ls="ls --color"

export SSH_AUTH_SOCK=${XDG_RUNTIME_DIR}/gnupg/S.gpg-agent.ssh


source /etc/profile.d/bash_completion.sh
for file in ~/.bashrc_includes/*
do
	source "${file}"
done


# automatically activate virtualenv when entering the directory
function virtualenv_auto_activate() {
	if [[ -z "${NO_AUTOVIRTUALENV+x}" ]] && [[ -e ".venv" ]]
	then
		# Check to see if already activated to avoid redundant activating
		if [[ "$VIRTUAL_ENV" != "$(pwd -P)/.venv" ]]
		then
			echo "Activating virtualenv..."
			source .venv/bin/activate
		fi
	fi
}
export PROMPT_COMMAND="virtualenv_auto_activate"


case "$(tty)" in
	/dev/tty[0-9]*)
		function parse_git_branch() {
			git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
		}
		export PS1="\[\e[01;32m\]\u@\h\[\e[01;34m\] \w\[\e[0;33m\]\$(parse_git_branch) \[\e[01;34m\]>\[\e[00m\\] "

		tty_basename="$(basename $(tty))"
		systemctl --user import-environment
		systemctl --user start "tty-session@${tty_basename}.target"
		;;
	*)
		alias sudo="sudo --askpass"

		if [[ -x "$(command -v powerline-shell)" ]]
		then
			function powerline_shell_prompt() {
				PS1=$(powerline-shell $?)
			}
			export PROMPT_COMMAND="${PROMPT_COMMAND}; powerline_shell_prompt"
		else
			export PS1="\[\e[01;32m\]\u@\h\[\e[01;34m\] \w\[\e[0;33m\]\$(parse_git_branch) \[\e[01;34m\]>\[\e[00m\\] "

		fi
		;;
esac


function history_prompt() {
	terminal_string="${TERMINAL} @ $(pwd -P)"
	entry=${BASH_COMMAND}
	if [[ "${entry}" == "powerline_shell_prompt" ]]
	then
		echo -n "${terminal_string}"
		echo "${terminal_string}" >> /tmp/debug
	else
		echo -n "${entry} (${terminal_string})"
		echo "${entry} (${terminal_string})" >> /tmp/debug
	fi
}

trap 'echo -ne "\033]2;$(history_prompt)\007"' DEBUG
