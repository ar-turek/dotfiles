# ~/.bash_logout: executed by bash(1) when login shell exits.

# when leaving the console clear the screen to increase privacy

if [ "$SHLVL" = 1 ]; then
    [ -x /usr/bin/clear_console ] && /usr/bin/clear_console -q

    case "$(tty)" in
        /dev/tty[0-9]*)
            tty_basename="$(basename $(tty))"
            systemctl --user stop "tty-session@${tty_basename}.target"
		    ;;
	    *)
		    ;;
    esac
fi
