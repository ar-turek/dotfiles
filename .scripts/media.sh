#!/usr/bin/env bash


status="$(playerctl status)"
current_track="$(playerctl metadata title) - $(playerctl metadata artist) ($(playerctl metadata album))"
if [[ "${status}" == "Playing" || "${status}" == "Paused" ]]
then
    title="${current_track:0:75}"

    if [[ "${status}" == "Playing" ]]
    then
        title=" ${title}"
        play_pause=""
    else
        title=" ${title}"
        play_pause=""
    fi
else
    title="Nothing is playing..."
    play_pause=""
fi


repeat=$(playerctl loop)
if [[ "${repeat}" == "None" ]]
then
    toggle_repeat="playerctl repeat Track"
    repeat=""
fi
if [[ "${repeat}" == "Track" ]]
then
    toggle_repeat="playerctl repeat Playlist"
    repeat=""
fi
if [[ "${repeat}" == "Playlist" ]]
then
    toggle_repeat="playerctl repeat None"
    repeat=""
fi


shuffle=$(playerctl shuffle)
if [[ "${shuffle}" == "Off" ]]
then
    toggle_shuffle="playerctl shuffle On"
    shuffle=""
fi
if [[ "${shuffle}" == "On" ]]
then
    toggle_shuffle="playerctl shuffle Off"
    shuffle=""
fi


previous=""
next=""
repeat=""
shuffle=""


options="${previous}\n${play_pause}\n${next}\n${repeat}\n${shuffle}"

chosen=$(echo -e ${options} | rofi -p "${title}" -dmenu -a 1 -a 2 -selected-row 1 -theme $HOME/.config/rofi/themes/media.rasi)
case "${chosen}" in
    "${previous}")
        playerctl previous
        ;;
    "${play_pause}")
        playerctl play-pause
        ;;
    "${next}")
        playerctl next
        ;;
    "${repeat}")
        ${toggle_repeat[@]}
        ;;
    "${shuffle}")
        ${toggle_shuffle[@]}
        ;;
esac
