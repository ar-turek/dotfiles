#!/usr/bin/env bash

command=$(rofi -dmenu -p ${1:-'Run:'} -theme ~/.config/rofi/themes/input.rasi)
${command[@]}
