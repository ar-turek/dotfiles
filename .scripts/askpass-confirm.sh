#!/usr/bin/env bash

password=$(~/.scripts/askpass.sh "Enter password:")

if [[ "${password}" != $(~/.scripts/askpass.sh "Confirm password:") ]]
then
    rofi -e "Passwords don't match!" -theme ~/.config/rofi/themes/error.rasi
    exit 1
else
    echo "${password}"
fi
