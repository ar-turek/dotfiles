#!/usr/bin/env bash

# produces an ffplay window with your desktop screen as a workaround for Wayland's lack of screen sharing

device=$(v4l2-ctl --list-devices | grep "Virtual Desktop Stream" -n1 | tail -1 | cut -d "-" -f 2 | tr -d " \n\r\t")
output=$(swaymsg -p -t get_outputs | grep focused | cut -d " " -f 2 | tr -d " \n\r\t")

wf-recorder --muxer=v4l2 --codec=rawvideo --file="${device}" -x yuv420p -o "${output}" &
recorder_pid=${!}

ffplay "${device}"

kill ${recorder_pid}
sleep 1

if ps -p ${recorder_pid} > /dev/null; then
    kill -9 ${recorder_pid}
fi
