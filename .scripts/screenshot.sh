#!/usr/bin/env bash

case "${1}" in
    screen_to_file)
        filename="$(date +'%s').png"
        full_file_path="${HOME}/Screenshots/${filename}"
        grim -t png "${full_file_path}" && notify-send --category=screenshots --urgency=low --expire-time=5000 --app-name=grim "Screenshot taken" "Screenshot saved to ${full_file_path}" --icon="${full_file_path}"
        ;;

    snap_to_file)
        filename="snap-$(date +'%s').png"
        grim -g "$(slurp)" -t png "${HOME}/Screenshots/${filename}" && notify-send --category=screenshots --urgency=low --expire-time=5000 --app-name=grim "Screen snap taken" "Snap saved to ${full_file_path}" --icon="${full_file_path}"
        ;;

    screen)
        full_file_path=$(mktemp /tmp/screenshot.XXXXXX.png)
        grim -t png "${full_file_path}" && wl-copy -t image/png < "${full_file_path}" && notify-send --category=screenshots --urgency=low --expire-time=5000 --app-name=grim "Screenshot taken" "Screenshot copied to clipboard" --icon="${full_file_path}"
        rm "${full_file_path}"
        ;;

    snap)
        full_file_path=$(mktemp /tmp/snap-screenshot.XXXXXX.png)
        grim -g "$(slurp)" -t png "${full_file_path}" && wl-copy -t image/png < "${full_file_path}" && notify-send --category=screenshots --urgency=low --expire-time=5000 --app-name=grim "Screen snap taken" "Snap copied to clipboard" --icon="${full_file_path}"
        rm "${full_file_path}"
        ;;

    *)
        ;;
esac
