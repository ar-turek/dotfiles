#!/usr/bin/env bash

rofi -dmenu -password -p "${1:-Enter password:}" -theme ~/.config/rofi/themes/input.rasi
