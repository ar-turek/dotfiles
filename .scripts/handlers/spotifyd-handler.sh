#!/usr/bin/env bash

case "${PLAYER_EVENT}" in
    change)
        metadata=$(playerctl metadata)
        title=$(echo -e "${metadata}" | grep 'xesam:title' | tr -s ' ' | cut -d ' ' -f 3-)
        artist=$(echo -e "${metadata}" | grep 'xesam:artist' | head -1 | tr -s ' ' | cut -d ' ' -f 3-)
        album=$(echo -e "${metadata}" | grep 'xesam:album' | grep -v 'xesam:albumArtist' | tr -s ' ' | cut -d ' ' -f 3-)
        icon=$(echo -e "${metadata}" | grep 'mpris:artUrl' | tr -s ' ' | cut -d ' ' -f 3-)
        thumbnail_file=$(mktemp /tmp/spotifyd.XXXXXX.png)
        wget -qO "${thumbnail_file}" ${icon}

        notify-send --category=spotifyd --urgency=low --expire-time=8000 --app-name=Spotify "${title}" "${artist} - ${album}" --icon="${thumbnail_file}"

        rm "${thumbnail_file}"
        ;;
    *)
        ;;
esac
