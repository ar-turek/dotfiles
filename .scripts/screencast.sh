#!/usr/bin/env bash

name="$(date +'%s')"
dir="${HOME}/Screencasts/${name}"
full_file_path="${dir}/${name}.mkv"

mkdir -p dir

$(wf-recorder -f "${full_file_path}" -c h264_vaapi -d /dev/dri/renderD128 -g "$(slurp)" --force-yuv)

# 2> /dev/null > /dev/null

notify-send --category=screencasts --urgency=low --expire-time=5000 --app-name=wf-recorder "Screencast recorded" "Screencast saved to ${dir}" --icon="${full_file_path}"
