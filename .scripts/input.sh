#!/usr/bin/env bash

rofi -dmenu -p "${1:-Input:}" -theme ~/.config/rofi/themes/input.rasi
