#!/usr/bin/env bash

command=$(rofi -dmenu -p ${1:-'Run in terminal:'} -theme ~/.config/rofi/themes/input.rasi)
alacritty -e ${command[@]}
